import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Articulo
{
    private String texto;
    private List<Comentarios> comentarios;
    public Usuario usuario;
    int like;
    int estrellas;
    List<Usuario> listaLikeArticulo;
    //List<Usuario> listaestrellas;

    public Articulo(String texto,Usuario Vusuario)
    {
        this.texto=texto;
        this.usuario=Vusuario;
        this.like=0;
        listaLikeArticulo=new LinkedList<>();
        comentarios=new LinkedList<>();
    }

    //public void BorrarComentario(int index)
    //{
     //   comentarios.remove(comentario);
    //}

    public void Like(Usuario nombre)
    {
        this.like++;
        listaLikeArticulo.add(nombre);
    }

    void MostrarArticulo()
    {
        System.out.println("Texto de Artiulo:"+texto+"\n");
        System.out.println("Dueno de Articulo:"+usuario.GetUsuario()+"\n");
        System.out.println("Like :"+like+"\n");

        System.out.println("ESTRELLAS 1 A 5 :"+Estrellas(usuario)+"\n");

        for (int i=0;i<listaLikeArticulo.size();i++)
        {
            System.out.println(" "+listaLikeArticulo.get(i).GetUsuario()+" ");
        }
    }

    public int numeroLetras()
    {
        int cont=0;
        Comentarios texto_articulo=new Comentarios(texto,usuario);
        for (int i=0;i<comentarios.size();i++)
        {
            cont=cont+comentarios.get(i).numeroLetras();
        }
        cont=cont + texto_articulo.numeroLetras();
        return cont;
    }

    public int Estrellas(Usuario nombre)
    {
        //listaestrellas.add(nombre);
        Scanner lectorEstrellas=new Scanner(System.in);
        System.out.println("Ingrese su rating: ");
        int rating=lectorEstrellas.nextInt();
        return rating;
    }

}

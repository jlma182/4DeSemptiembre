public class SitioWeb {

    String url="www.blog.com";
    String nombre="Blogs";

    public String getCompleteUrl()
    {
        return url;
    }

    public String getCompleteNombre()
    {
        return nombre;
    }
    public void setUrl(String Vurl)
    {
        url=Vurl;
    }

    public void setNombre(String Vnombre)
    {
        nombre=Vnombre;
    }

    public void MostrarSitio()
    {
        System.out.printf("pagina: "+url);
        System.out.printf("      ");
        System.out.printf("nombre: "+nombre+"\n");
    }
}

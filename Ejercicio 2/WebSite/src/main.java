public class main {

    public static void main(String[] args)
    {
        SitioWeb s1 = new SitioWeb();
        Blog b1 = new Blog("todo","all.io");

        Usuario u1=new Usuario("Jorge","Mazuelo");
        Usuario u2=new Usuario("Pancho","Villa");
        Usuario u3=new Usuario("Maskon","Lopez");

        Articulo a1 = new Articulo("Libro", u1);
        Articulo a2 = new Articulo("Reloj", u3);

        Comentarios c1=new Comentarios("LIBRO BUENO",u1);

        //a1.Like(u2);
        //c1.Like(u2);

        s1.MostrarSitio();
        b1.mostrarBlog();

        a1.MostrarArticulo();
        a2.MostrarArticulo();

        c1.mostrarComentario();
        System.out.println(c1.numeroLetras());

    }
}
